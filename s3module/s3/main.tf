resource "aws_s3_bucket" "s3_test" {
  bucket = var.s3_bucket_name
  acl    = var.acl
  versioning {
    enabled = true
  }
  tags = {
    Name = var.s3_bucket_name
  }

  logging {
    target_bucket = var.target_bucket.id
    target_prefix = "log/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kms_master_key_id.id
        sse_algorithm     = "aws:kms"
      }
    }
  }
}