variable "s3_bucket_name" {
  type = string
  description = "Enter s3 bucket name"
}

variable "acl" {
  type = string
  default = "private"
  description = "Enter acl"
}

variable "target_bucket" {
  type = string
  description = "Enter the name of bucket where you want logs to be forwarded to(Imported Value)"
}

variable "kms_master_key_id" {
  description = "Enter the name of the kmskey needed to encrypt this s3 bucket(Imported Value)"
}

variable "regoin" {
  type = string
}

